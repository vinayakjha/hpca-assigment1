#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width];

        //Handling corner cases
        // h = 0 and w = 0 single element
        copy[0] += image[0] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1]
                    + image[1] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2]
                    + image[(1 * width) + 0] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1]
                    + image[(1 * width) + 1] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2];

        // h = 0 and w < width - 1
        
                        for(int w = 1; w < width/4; w++)
                        {
                            __builtin_prefetch((int*)(image+1024),0,0);
                            copy[w] += image[(0) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                    image[(0) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] + 
                                    image[(0) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                    image[(1) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE] +
                                    image[(1) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1] +
                                    image[(1) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]    ;
                        }

        
                        for(int w = width/4; w < (2*width)/4; w++)
                        {
                            
                            copy[w] += image[(0) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                    image[(0) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] + 
                                    image[(0) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                    image[(1) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE] +
                                    image[(1) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1] +
                                    image[(1) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]    ;
                        }
                    
                

        
                        for(int w = (2*width)/4; w < (3*width)/4; w++)
                        {
                            
                            copy[w] += image[(0) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                    image[(0) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] + 
                                    image[(0) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                    image[(1) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE] +
                                    image[(1) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1] +
                                    image[(1) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]    ;
                        }
                    
                

        
                        for(int w = (3*width)/4; w < width - 1; w++)
                        {
                            
                            copy[w] += image[(0) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                    image[(0) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] + 
                                    image[(0) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                    image[(1) * width - 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE] +
                                    image[(1) * width + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1] +
                                    image[(1) * width + 1 + w] * 
                                    filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]    ;
                        }
                    
                
        // h = 0 and w = width - 1
        copy[width - 1] += image[width - 2] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 0]
                    + image[width - 1] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1]
                    + image[(1 * width) + (width - 2)] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 0]
                    + image[(1 * width) + width - 1] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1];
        // h < height - 1
        for(int h = 1; h < height-1; h++)
        {  
                //width = 0
                        __builtin_prefetch((int*)(image+1024),0,0);  
                        copy[h * width + 0] += image[(h - 1) * width] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                            image[(h - 1) * width + 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2] +
                                            image[(h) * width] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 1] +
                                            image[(h) * width + 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 2] +
                                            image[(h + 1) * width] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 1] +
                                            image[(h + 1) * width + 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 2];
         
                //w < width - 1
               
                        for(int w = 1; w < width/4; w++)
                        {
                            
                            __builtin_prefetch((int*)(image+1024),0,0);
                            copy[h * width + w] += image[(h - 1) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE] +
                                            image[(h - 1) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                            image[(h - 1) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE  +2] +
                                            image[(h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                            image[(h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] +
                                            image[(h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                            image[(1 + h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE] +
                                            image[(1 + h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 1] +
                                            image[(1 + h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 2]        
                            
                            ;
                        }

                
                        for(int w = width/4; w < (2*width)/4; w++)
                        {
                            __builtin_prefetch((int*)(image+(1024*4)),0,0);
                            copy[h * width + w] += image[(h - 1) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE] +
                                            image[(h - 1) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                            image[(h - 1) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE  +2] +
                                            image[(h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                            image[(h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] +
                                            image[(h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                            image[(1 + h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE] +
                                            image[(1 + h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 1] +
                                            image[(1 + h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 2]        
                            
                            ;
                        }

            
                        for(int w = (2*width)/4; w < (3*width)/4; w++)
                        {
                            __builtin_prefetch((int*)(image+(1024*8)),0,0);
                            copy[h * width + w] += image[(h - 1) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE] +
                                            image[(h - 1) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                            image[(h - 1) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE  +2] +
                                            image[(h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                            image[(h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] +
                                            image[(h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                            image[(1 + h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE] +
                                            image[(1 + h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 1] +
                                            image[(1 + h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 2]        
                            
                            ;
                        }

                        for(int w = (3*width)/4; w < width-1; w++)
                        {
                            __builtin_prefetch((int*)(image+(1024*12)),0,0);
                           copy[h * width + w] += image[(h - 1) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE] +
                                            image[(h - 1) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                            image[(h - 1) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE  +2] +
                                            image[(h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE] +
                                            image[(h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1] +
                                            image[(h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2] +
                                            image[(1 + h) * width + w - 1] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE] +
                                            image[(1 + h) * width + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 1] +
                                            image[(1 + h) * width + 1 + w] * 
                                            filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 2]        
                            
                            ;
                        }



                // h < height - 1 and w = width - 1
  

                            copy[h * width + (width-1)] += image[(h - 1) * width + (width-1) - 1] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE] +
                                                        image[(h - 1) * width + (width-1)] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                                        image[(h) * width - 1 + (width-1)] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE] +
                                                        image[(h) * width + (width-1)] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 1] +
                                                        image[(1 + h) * width - 1 + (width-1)] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE] +
                                                        image[(1 + h) * width + (width-1)] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 2 * FILTER_SIZE + 1];
                            ;

        }
                
        
        // h = height - 1 and w = 0
        copy[(height-1)*width] += image[(height-2)*width + 0] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1]
                    + image[(height-2)*width + 1] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 2]
                    + image[(height-1)*width] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1]
                    + image[(height-1)*width + 1] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2];
        // h = height - 1 and w < width - 1
                
    /*here*/                    for(int w = 1; w < width/4; w++)
                        {
                            __builtin_prefetch((int*)(image+1024),0,0);
                            
                            copy[(height-1) * width + w] += image[(height-2) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE] +
                                                        image[(height-2) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                                        image[(height-2) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 2] +
                                                        image[(height-1) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE] +
                                                        image[(height-1) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 1] +
                                                        image[(height-1) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 2];
                        }


    /*here*/             for(int w = width/4; w < (2*width)/4; w++)
                        {
                            __builtin_prefetch((int*)(image+(1024*4)),0,0);
                           copy[(height-1) * width + w] += image[(height-2) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE] +
                                                        image[(height-2) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                                        image[(height-2) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 2] +
                                                        image[(height-1) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE] +
                                                        image[(height-1) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 1] +
                                                        image[(height-1) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 2];
                        }

    /*here*/            for(int w = (2*width)/4; w < (3*width)/4; w++)
                        {
                            __builtin_prefetch((int*)(image+(1024*8)),0,0);
                            copy[(height-1) * width + w] += image[(height-2) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE] +
                                                        image[(height-2) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                                        image[(height-2) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 2] +
                                                        image[(height-1) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE] +
                                                        image[(height-1) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 1] +
                                                        image[(height-1) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 2];
                        }

         for(int w = (3*width)/4; w < width-1; w++)
                        {
                            __builtin_prefetch((int*)(image+(1024*12)),0,0);
                            copy[(height-1) * width + w] += image[(height-2) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE] +
                                                        image[(height-2) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1] +
                                                        image[(height-2) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 2] +
                                                        image[(height-1) * width - 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE] +
                                                        image[(height-1) * width + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 1] +
                                                        image[(height-1) * width + 1 + w] * 
                                                        filter[i * FILTER_SIZE * FILTER_SIZE + 1 * FILTER_SIZE + 2];
                        }

        // h = height - 1 and w = width - 1
        copy[height*width - 1] += image[(height-1)*width - 2] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 0]
                    + image[(height-1)*width - 1] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1]
                    + image[height*width - 2] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 0]
                    + image[(height*width) - 1] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1];

        

    for(int h = height-1; h > -1; --h)
        {
            for(int w = width -1; w >-1; --w)
                image[h * width + w] = copy[h * width + w];
        }
        delete copy;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];

    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];

    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output_modified.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
